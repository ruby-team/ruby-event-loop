require "rake/packagetask"

begin
  require "rubygems"
  require "rake/gempackagetask"
  $have_rubygems = true
rescue
  $have_rubygems = false
end

PKG_VERSION = File.open("VERSION").read.chomp
PKG_FILES = FileList[*%w{[A-Z]* README setup.rb lib/**/*.rb}]

MAINTAINER_MAILNAME = "wigwam.brockman.se"
SCP_TARGET = "teepee:/var/www/www.brockman.se/software/ruby-event-loop/"

task :default => ["README", :check]

file "README" => ["README.utf-8", "Rakefile"] do |t|
  # I'm paranoid and I don't trust Ruby with Unicode.
  if "a—b".gsub(/—/, "---") != "a---b"
    warn "Can't build `README': broken Unicode support."
  else
    File.open("README", "w") do |out|
      out << "\
------------------------------------------------------------
 This file was automatically generated from `README.utf-8'.
------------------------------------------------------------\n\n"
      IO.foreach("README.utf-8") do |line|
        out << line.
          gsub(/©/, "(C)").
          gsub(/—/, "---").
          gsub(/‘/, "`").
          gsub(/’/, "'").
          gsub(/“/, "\"").
          gsub(/”/, "\"")
      end
    end
  end
end

task :check do
  Dir.foreach("lib/event-loop/") do |basename|
    next if basename[0] == ?.
    system %{ruby -w -Ilib "lib/event-loop/#{basename}"}
  end
end

if $have_rubygems
  GEMSPEC = Gem::Specification.new do |gem|
    gem.name = "event-loop"
    gem.version = PKG_VERSION
    gem.summary = "Simple and usable event loop and signal system"
    gem.description =
      "This package contains a simple select-based event loop " +
      "featuring IO (file descriptor) and timer event sources.  " + 
      "It comes with a signal system inspired by that of GLib."
    gem.files = PKG_FILES.to_a
    gem.require_path = "lib"
    gem.author = "Daniel Brockman"
    gem.email = "daniel@brockman.se"
    gem.homepage = "http://www.brockman.se/software/ruby-event-loop/"
  end

  Rake::GemPackageTask.new(GEMSPEC) do |task|
    task.need_tar_gz = true
  end
else
  Rake::PackageTask.new("event-loop", PKG_VERSION) do |task|
    task.need_tar_gz = true
    task.package_files = PKG_FILES
  end
end

task :upload => :package do
  mailname = File.read("/etc/mailname").chomp rescue "(none)"
  if mailname == MAINTAINER_MAILNAME
    sh %{scp -r pkg/*#{PKG_VERSION}* #{SCP_TARGET}}
  else
    warn "The `upload' target is only meant for the maintainer."
  end
end
