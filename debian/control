Source: ruby-event-loop
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Gunnar Wolf <gwolf@debian.org>,
           Lucas Nussbaum <lucas@debian.org>
Build-Depends: debhelper (>= 9~),
               gem2deb
Standards-Version: 3.9.7
Vcs-Git: https://anonscm.debian.org/git/pkg-ruby-extras/ruby-event-loop.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-ruby-extras/ruby-event-loop.git
Homepage: http://brockman.se/2005/ruby-event-loop/
XS-Ruby-Versions: all

Package: ruby-event-loop
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ${misc:Depends},
         ${shlibs:Depends}
Provides: libevent-loop-ruby,
          libevent-loop-ruby1.8
Replaces: libevent-loop-ruby (<< 0.3-4~),
          libevent-loop-ruby1.8 (<< 0.3-4~)
Conflicts: libevent-loop-ruby (<< 0.3-4~),
           libevent-loop-ruby1.8 (<< 0.3-4~)
Description: simple signal system and an event loop for Ruby
 This is a library for building event-based programs with Ruby. It contains
 both a simple signal system and an event loop that uses said signal system.

Package: libevent-loop-ruby
Section: oldlibs
Architecture: all
Depends: ruby-event-loop,
         ${misc:Depends}
Description: Transitional package for ruby-event-loop
 This is a transitional package to ease upgrades to the ruby-event-loop
 package. It can safely be removed.

Package: libevent-loop-ruby1.8
Section: oldlibs
Architecture: all
Depends: ruby-event-loop,
         ${misc:Depends}
Description: Transitional package for ruby-event-loop
 This is a transitional package to ease upgrades to the ruby-event-loop
 package. It can safely be removed.
